﻿#BookingApp

This is Booking App based on Springboot 2.0 (Spring 5.0), Hibernate and mysql db.  
  
####Booking flow:
Customer calls POST /api/v1/room/{room}/book which creates unconfirmed booking  
Scheduled task fetches all unconfirmed bookings, and accepts or rejects them one by one - this is to prevent race condition when 2 customers create booking with overlapping period in 2 separate DB transactions  
In real app customer would get i.e email notification once his booking gets accepted or rejected.  

##Getting Started

### Prerequisites
Mysql database (preferably on http://localhost:3306 with user 'root' and password 'root')  
Docker command: `docker run -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=root -d mysql:latest`

### Installing
Check/update database settings (url, user, password) in 'src/main/resources/application.properties'   
Init database with `/src/main/db/db-schema.sql` script  
Optionally load some test data from `/src/main/db/test-data.sql`  

## Building and running

### Running app
`mvn compile exec:java`  

### Running all the tests (unit and integration/api acceptance tests)
`mvn clean verify -fae`  

### Building fat jar
`mvn clean test package`  
JAR is in ./target folder  

### Api documentation (Swagger 2) 
First run the app, then goto [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)  
