package com.lukaszwitkowski.bookingapp.customer.domain;

import com.lukaszwitkowski.bookingapp.customer.repository.CustomerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by lwitkowski on 10.04.2018
 */
@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {
    @Mock
    private CustomerRepository customerRepository;

    private CustomerService customerService;
    
    private Customer customer;
    
    @Before
    public void setUp() {
        customer = new Customer();
        customerService = new CustomerService(customerRepository);
    }

    @Test
    public void testRegister() {
        Customer newCustomer = customerService.registerCustomer("bb@cc.dd");
        assertThat(newCustomer.getEmail(), is("bb@cc.dd"));
    }

    @Test(expected = EmailAlreadyRegisteredException.class)
    public void testRegisterEmailAlreadyRegistered() {
        when(customerRepository.saveAndFlush(ArgumentMatchers.any(Customer.class))).thenThrow(new DataIntegrityViolationException(""));
        customerService.registerCustomer("aa@bb.cc");
    }
}
