package com.lukaszwitkowski.bookingapp.booking.domain;

import com.google.common.collect.Lists;
import com.lukaszwitkowski.bookingapp.booking.repository.BookingRepository;
import com.lukaszwitkowski.bookingapp.booking.repository.RoomRepository;
import com.lukaszwitkowski.bookingapp.customer.domain.Customer;
import com.lukaszwitkowski.bookingapp.customer.repository.CustomerRepository;
import com.lukaszwitkowski.bookingapp.utils.ValidationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by lwitkowski on 10.04.2018
 */
@RunWith(MockitoJUnitRunner.class)
public class BookingServiceTest {
    @Mock
    private BookingRepository bookingRepository;
    @Mock
    private RoomRepository roomRepository;
    @Mock
    private CustomerRepository customerRepository;

    private BookingService bookingService;
    
    private Customer customer;
    private Room room;
    private Booking booking1;
    private Booking booking2;
    private Booking booking3;
    
    @Before
    public void setUp() {
        customer = new Customer();
        room = new Room();
        booking1 = new Booking(customer, room, LocalDate.of(2018,1, 1), LocalDate.of(2018,1, 3));
        booking2 = new Booking(customer, room, LocalDate.of(2018,1, 1), LocalDate.of(2018,1, 1));
        booking3 = new Booking(customer, room, LocalDate.of(2018,1, 4), LocalDate.of(2018,1, 5));
        
        when(bookingRepository.findFirst10ByStatus(BookingStatus.PENDING))
                .thenReturn(Lists.newArrayList(booking1, booking2, booking3));
        
        when(bookingRepository.countConfirmedForRoomAndDateRange(booking1.getRoom(), booking1.getDateFrom(), booking1.getDateTo()))
                .thenReturn(0l);
        when(bookingRepository.countConfirmedForRoomAndDateRange(booking2.getRoom(), booking2.getDateFrom(), booking2.getDateTo()))
                .thenReturn(1l);
        when(bookingRepository.countConfirmedForRoomAndDateRange(booking3.getRoom(), booking3.getDateFrom(), booking3.getDateTo()))
                .thenReturn(0l);

        when(customerRepository.findById(customer.getId())).thenReturn(Optional.of(customer));
        
        bookingService = new BookingService(bookingRepository, roomRepository, customerRepository);
    }


    @Test
    public void testCreateBookingDayAfter() {
        Booking booking = bookingService.createBooking(room, customer.getId(), LocalDate.of(2017,2, 1), 
                LocalDate.of(2019,2, 1));
        assertThat(booking, is(notNullValue()));
    }
    @Test
    public void testCreateBookingDayAfterOccupied() {
        Booking booking = bookingService.createBooking(room, customer.getId(), LocalDate.of(2018,1, 6), 
                LocalDate.of(2018,1, 6));
        assertThat(booking, is(notNullValue()));    
    }

    @Test(expected = BookingConflictException.class)
    public void testCreateBookingConflict() {
        LocalDate from = LocalDate.of(2017,2, 1);
        LocalDate to = LocalDate.of(2020,2, 1);
        when(bookingRepository.countNotRejectedForRoomAndDateRange(room,from, to)).thenReturn(1l);
        bookingService.createBooking(room, customer.getId(), from, to);
    }

    @Test(expected = ValidationException.class)
    public void testCreateBookingUnknownCustomer() {
        LocalDate from = LocalDate.of(2017,2, 1);
        LocalDate to = LocalDate.of(2020,2, 1);
        bookingService.createBooking(room, -1l, from, to);
    }
    
    @Test(expected = ValidationException.class)
    public void testCreateBookingDatesValidation() {
        LocalDate from = LocalDate.of(2017,2, 2);
        LocalDate to = LocalDate.of(2017,2, 1);
        bookingService.createBooking(room, customer.getId(), from, to);
    }
    
    @Test
    public void testProcessPendingBookings() {
        bookingService.processPendingBookings();
        assertThat(booking1.getStatus(), is(BookingStatus.CONFIRMED));
        assertThat(booking2.getStatus(), is(BookingStatus.REJECTED));
        assertThat(booking3.getStatus(), is(BookingStatus.CONFIRMED));
    }
}
