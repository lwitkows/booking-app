package com.lukaszwitkowski.bookingapp.rest;

import com.lukaszwitkowski.bookingapp.booking.domain.Booking;
import com.lukaszwitkowski.bookingapp.booking.domain.Hotel;
import com.lukaszwitkowski.bookingapp.booking.domain.Room;
import com.lukaszwitkowski.bookingapp.booking.repository.BookingRepository;
import com.lukaszwitkowski.bookingapp.booking.repository.HotelRepository;
import com.lukaszwitkowski.bookingapp.booking.repository.RoomRepository;
import com.lukaszwitkowski.bookingapp.customer.domain.Customer;
import com.lukaszwitkowski.bookingapp.customer.repository.CustomerRepository;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

/**
 * Created by lwitkowski on 10.04.2018
 */
@RunWith(SpringRunner.class)
public class RoomSearchRestIT extends AbstractRestIT {

    public static final String EMAIL = "contact@lukaszwitkowski.com";

    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private HotelRepository hotelRepository;
    
    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private BookingRepository bookingRepository;
    
    @Before
    public void setUp() {
        Customer customer = customerRepository.save(new Customer(EMAIL));
        
        Hotel hotel1 = hotelRepository.save(new Hotel("Warsaw"));
        Hotel hotel2 = hotelRepository.save(new Hotel("Warsaw"));
        Hotel hotel3 = hotelRepository.save(new Hotel("Cracow"));
        
        Room room1 = roomRepository.save(new Room(hotel1, 3));
        Room room2 = roomRepository.save(new Room(hotel1, 6));
        Room room3 = roomRepository.save(new Room(hotel1, 9));
        Room room4 = roomRepository.save(new Room(hotel1, 12));
        Room room5 = roomRepository.save(new Room(hotel1, 15));
        Room room6 = roomRepository.save(new Room(hotel2, 18));
        Room room7 = roomRepository.save(new Room(hotel3, 21));

        bookingRepository.save(new Booking(customer, room1, LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 3)));
        bookingRepository.save(new Booking(customer, room1, LocalDate.of(2018, 1, 7), LocalDate.of(2018, 1, 31)));
        bookingRepository.save(new Booking(customer, room1, LocalDate.of(2018, 2, 1), LocalDate.of(2018, 2, 10)));
        bookingRepository.save(new Booking(customer, room7, LocalDate.of(2012, 2, 1), LocalDate.of(2012, 3, 10)));
    }

    @After
    public void tearDown() {
        hotelRepository.deleteAll();
        customerRepository.deleteAll();
    }

    @Test
    public void testRoomSearchByPrice() {
        given()
                .get("/api/v1/room/search?priceFrom=10&priceTo=30&dateFrom=1990-01-01&dateTo=1990-01-01")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("totalElements", is(4));
    }

    @Test
    public void testRoomSearchByPriceEmpty() {
        given()
                .get("/api/v1/room/search?priceFrom=30&dateFrom=1990-01-01&dateTo=1990-01-01")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("totalElements", is(0));
    }


    @Test
    public void testRoomSearchByPriceInvalidParams() {
        given()
                .get("/api/v1/room/search?priceFrom=30&priceTo=10&dateFrom=1990-01-01&dateTo=1990-01-01")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }
    
    @Test
    public void testRoomSearchByCity() {
        given()
                .get("/api/v1/room/search?city=Warsaw&dateFrom=1990-01-01&dateTo=1990-01-01")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("totalElements", is(6));
    }
    
    @Test
    public void testRoomSearchByUnknownCity() {
        given()
                .get("/api/v1/room/search?city=Poznan&dateFrom=1990-01-01&dateTo=1990-01-01")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("totalElements", is(0));
    }
    
    @Test
    public void testRoomSearchByDate() {
        given()
                .get("/api/v1/room/search?dateFrom=2019-01-01&dateTo=2019-01-31")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("totalElements", is(7));
    }
    
    @Test
    public void testRoomSearchByDate2() {
        given()
                .get("/api/v1/room/search?dateFrom=2018-02-10&dateTo=2019-01-31")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("totalElements", is(6));
    }

    @Test
    public void testRoomSearchByDate3() {
        given()
                .get("/api/v1/room/search?dateFrom=2010-01-01&dateTo=2030-01-31")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("totalElements", is(5));
    }

    @Test
    public void testRoomSearchByDateInvalidParams() {
        given()
                .get("/api/v1/room/search?dateFrom=2018-01-01&dateTo=2017-01-31")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }
}