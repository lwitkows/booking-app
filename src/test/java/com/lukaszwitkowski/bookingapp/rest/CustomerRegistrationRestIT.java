package com.lukaszwitkowski.bookingapp.rest;

import com.lukaszwitkowski.bookingapp.customer.domain.Customer;
import com.lukaszwitkowski.bookingapp.customer.repository.CustomerRepository;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Created by lwitkowski on 10.04.2018
 */
@RunWith(SpringRunner.class)
public class CustomerRegistrationRestIT extends AbstractRestIT {

    public static final String EMAIL = "contact@lukaszwitkowski.com";
    public static final String EMAIL_2 = "contact2@lukaszwitkowski.com";

    @Autowired
    private CustomerRepository customerRepository;

    @Before
    public void setUp() {
        customerRepository.save(new Customer(EMAIL_2));
    }

    @After
    public void tearDown() {
        customerRepository.deleteAll();
    }
    
    @Test
    public void testRegister() throws JSONException {
        given()
                .body(new JSONObject().put("email", EMAIL).toString())
                .post("/api/v1/customer/register")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("id", is(notNullValue()))
                .body("email", is(EMAIL));
    }

    @Test
    public void testRegisterDuplicateEmailConflict() throws JSONException {
        given()
                .body(new JSONObject().put("email", EMAIL_2).toString())
                .post("/api/v1/customer/register")
                .then()
                .statusCode(HttpStatus.SC_CONFLICT)
                .body("message", is("Email already registered"));
    }
    
    @Test
    public void testRegisterEmailValidation() throws JSONException {
        given()
                .body(new JSONObject().put("email", "aaaa@").toString())
                .post("/api/v1/customer/register")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }
}