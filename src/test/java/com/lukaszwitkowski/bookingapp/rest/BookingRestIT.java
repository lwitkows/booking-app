package com.lukaszwitkowski.bookingapp.rest;

import com.lukaszwitkowski.bookingapp.booking.domain.Booking;
import com.lukaszwitkowski.bookingapp.booking.domain.BookingStatus;
import com.lukaszwitkowski.bookingapp.booking.domain.Hotel;
import com.lukaszwitkowski.bookingapp.booking.domain.Room;
import com.lukaszwitkowski.bookingapp.booking.repository.BookingRepository;
import com.lukaszwitkowski.bookingapp.booking.repository.HotelRepository;
import com.lukaszwitkowski.bookingapp.booking.repository.RoomRepository;
import com.lukaszwitkowski.bookingapp.customer.domain.Customer;
import com.lukaszwitkowski.bookingapp.customer.repository.CustomerRepository;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Created by lwitkowski on 10.04.2018
 */
@RunWith(SpringRunner.class)
public class BookingRestIT extends AbstractRestIT {

    public static final String EMAIL = "contact@lukaszwitkowski.com";
    public static final String EMAIL_2 = "contact2@lukaszwitkowski.com";

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private BookingRepository bookingRepository;
    
    private Customer customer1;
    private Customer customer2;
    private Room room1;
    private Booking booking1;

    @Before
    public void setUp() {
        customer1 = customerRepository.save(new Customer(EMAIL));
        customer2 = customerRepository.save(new Customer(EMAIL_2));

        Hotel hotel1 = hotelRepository.save(new Hotel("Warsaw"));
        Hotel hotel2 = hotelRepository.save(new Hotel("Warsaw"));
        Hotel hotel3 = hotelRepository.save(new Hotel("Cracow"));

        room1 = roomRepository.save(new Room(hotel1, 3));
        Room room2 = roomRepository.save(new Room(hotel1, 6));
        Room room3 = roomRepository.save(new Room(hotel1, 9));
        Room room4 = roomRepository.save(new Room(hotel1, 12));
        Room room5 = roomRepository.save(new Room(hotel1, 15));
        Room room6 = roomRepository.save(new Room(hotel2, 18));
        Room room7 = roomRepository.save(new Room(hotel3, 21));

        booking1 = bookingRepository.save(new Booking(customer1, room1, LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 3)));
        bookingRepository.save(new Booking(customer1, room1, LocalDate.of(2018, 1, 7), LocalDate.of(2018, 1, 31)));
        bookingRepository.save(new Booking(customer1, room1, LocalDate.of(2018, 2, 1), LocalDate.of(2018, 1, 10)));
        bookingRepository.save(new Booking(customer1, room1, LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 3), BookingStatus.REJECTED));

    }

    @After
    public void tearDown() {
        hotelRepository.deleteAll();
        customerRepository.deleteAll();
    }

    @Test
    public void testBookOk() throws JSONException {
        given()
                .body(new JSONObject()
                        .put("customer", customer1.getId())
                        .put("dateFrom", "2019-01-01")
                        .put("dateTo", "2019-01-03")
                        .toString())
                .post("/api/v1/room/" +room1.getId()+ "/book")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("id", is(notNullValue()));
    }

    @Test
    public void testBookConflict() throws JSONException {
        given()
                .body(new JSONObject()
                        .put("customer", customer1.getId())
                        .put("dateFrom", "2018-01-03")
                        .put("dateTo", "2018-01-04")
                        .toString())
                .post("/api/v1/room/" +room1.getId()+ "/book")
                .then()
                .statusCode(HttpStatus.SC_CONFLICT);
    }

    @Test
    public void testBookConflict2() throws JSONException {
        given()
                .body(new JSONObject()
                        .put("customer", customer1.getId())
                        .put("dateFrom", "2019-01-01")
                        .put("dateTo", "2019-01-03")
                        .toString())
                .post("/api/v1/room/" +room1.getId()+ "/book")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("id", is(notNullValue()));
        
        given()
                .body(new JSONObject()
                        .put("customer", customer1.getId())
                        .put("dateFrom", "2019-01-01")
                        .put("dateTo", "2019-01-03")
                        .toString())
                .post("/api/v1/room/" +room1.getId()+ "/book")
                .then()
                .statusCode(HttpStatus.SC_CONFLICT);
    }

    @Test
    public void testBookInvalidParams() throws JSONException {
        given()
                .body(new JSONObject()
                        .put("customer", customer1.getId())
                        .put("dateFrom", "2020-01-01")
                        .put("dateTo", "2019-01-03")
                        .toString())
                .post("/api/v1/room/" +room1.getId()+ "/book")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }
    
    @Test
    public void testCustomerBookings() {
        given()
                .get("/api/v1/customer/"+customer1.getId()+"/booking")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("$", hasSize(4));
    }

    @Test
    public void testCustomerBookings2() throws JSONException {
        given()
                .get("/api/v1/customer/"+customer2.getId()+"/booking")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("$", hasSize(0));
        
        given()
                .body(new JSONObject()
                        .put("customer", customer2.getId())
                        .put("dateFrom", "2019-01-01")
                        .put("dateTo", "2019-01-03")
                        .toString())
                .post("/api/v1/room/" +room1.getId()+ "/book")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("id", is(notNullValue()));
        
        given()
                .get("/api/v1/customer/"+customer2.getId()+"/booking")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("$", hasSize(1));
    }

    @Test
    public void testRoomBookings() {
        given()
                .get("/api/v1/room/"+room1.getId()+"/booking")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("$", hasSize(4));
    }

    @Test
    public void testCancelBooking() {
        given()
                .delete("/api/v1/booking/" + booking1.getId())
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void testCancelBookingNotFound() {
        given()
                .delete("/api/v1/booking/" + booking1.getId())
                .then()
                .statusCode(HttpStatus.SC_OK);
        
        given()
                .delete("/api/v1/booking/" + booking1.getId())
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }
}