INSERT INTO customer (id, email) VALUES
	(1, 'contact@lukaszwitkowski.com');
	
INSERT INTO hotel (id, city) VALUES
    (1, 'Warsaw'),
    (2, 'Cracow');
    
INSERT INTO room (id, hotel_id, price) VALUES
    (1, 1, 10),
    (2, 1, 15),
    (3, 2, 12);
    
INSERT INTO booking (id, room_id, customer_id, date_from, date_to, status) VALUES
    (1, 1, 1, '2018-01-01', '2018-01-30', 'CONFIRMED'),
    (2, 1, 1, '2018-02-15', '2018-02-16', 'PENDING'),
    (3, 1, 1, '2018-01-20', '2018-01-30', 'REJECTED');