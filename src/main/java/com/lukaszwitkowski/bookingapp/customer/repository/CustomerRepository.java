package com.lukaszwitkowski.bookingapp.customer.repository;

import com.lukaszwitkowski.bookingapp.customer.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lwitkowski on 10.04.2018.
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {}
