package com.lukaszwitkowski.bookingapp.customer.domain;

import com.lukaszwitkowski.bookingapp.customer.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lwitkowski on 10.04.2018
 */
@Service
public class CustomerService {
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Transactional
    public Customer registerCustomer(String email) {
        try {
            Customer customer = new Customer(email);
            customerRepository.saveAndFlush(customer);
            return customer;
        } catch(DataIntegrityViolationException e) {
            throw new EmailAlreadyRegisteredException(email);
        }
    }
}