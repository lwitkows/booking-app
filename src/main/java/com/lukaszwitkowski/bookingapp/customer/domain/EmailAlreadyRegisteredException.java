package com.lukaszwitkowski.bookingapp.customer.domain;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by lwitkowski on 10.04.2018
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class EmailAlreadyRegisteredException extends RuntimeException {
    private final String email;

    public EmailAlreadyRegisteredException(String email) {
        super("Email already registered");
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
