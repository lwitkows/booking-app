package com.lukaszwitkowski.bookingapp.customer.rest;

import com.lukaszwitkowski.bookingapp.customer.domain.Customer;

/**
 * Created by lwitkowski on 10.04.2018.
 */
public class CustomerTO {

    private final Long id;
    private final String email;

    public CustomerTO(Customer customer) {
        this.id = customer.getId();
        this.email = customer.getEmail();
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }
}
