package com.lukaszwitkowski.bookingapp.customer.rest;

import com.lukaszwitkowski.bookingapp.booking.rest.BookingTO;
import com.lukaszwitkowski.bookingapp.customer.domain.Customer;
import com.lukaszwitkowski.bookingapp.customer.domain.CustomerService;
import com.lukaszwitkowski.bookingapp.utils.NotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by lwitkowski on 10.04.2018
 */
@RestController
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @RequestMapping(value = "/api/v1/customer/register", method = RequestMethod.POST)
    @ApiOperation("Registers new customer and returns it")
    @ApiResponse(code = 409, message = "Email already registered")
    public CustomerTO register(@RequestBody @Valid RegisterCustomerRequest registerCustomerRequest) {
        return new CustomerTO(
                customerService.registerCustomer(registerCustomerRequest.getEmail())
        );
    }

    @RequestMapping(value = "/api/v1/customer/{customer}/booking", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Returns customer's all bookings")
    @ApiResponse(code = 404, message = "Customer not found")
    public Collection<BookingTO> getCustomerBookings(@PathVariable("customer") Customer customer) {
        if (customer == null) {
            throw new NotFoundException("customer not found");
        }
        return customer.getBookings().stream()
                .map(BookingTO::withRoom)
                .collect(Collectors.toList());
    }
}