package com.lukaszwitkowski.bookingapp.customer.rest;

import javax.validation.constraints.Email;

/**
 * Created by lwitkowski on 10.04.2018.
 */
public class RegisterCustomerRequest {

    @Email
    private String email;

    public RegisterCustomerRequest() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
