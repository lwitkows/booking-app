package com.lukaszwitkowski.bookingapp.version;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by lwitkowski on 10.04.2018
 */
public class VersionResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String application;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String version;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String revision;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String buildDate;

    public VersionResponse(String applicationName, String version, String revision, String buildDate) {
        this.application = applicationName;
        this.version = version;
        this.revision = revision;
        this.buildDate = buildDate;
    }

    public String getApplication() {
        return application;
    }

    public String getVersion() {
        return version;
    }

    public String getRevision() {
        return revision;
    }

    public String getBuildDate() {
        return buildDate;
    }
}