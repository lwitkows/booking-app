package com.lukaszwitkowski.bookingapp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by lwitkowski on 10.04.2018
 */
@Configuration
@EnableJpaRepositories("com.lukaszwitkowski.bookingapp")
@EnableTransactionManagement
@PropertySource("classpath:version.properties")
@EnableScheduling
public class ApplicationConfig implements WebMvcConfigurer {

    @Value("${api.cors.allowed.origins}")
    private String corsAllowedOrigins;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        if (corsAllowedOrigins != null) {
            registry.addMapping("/**")
                    .allowedOrigins(corsAllowedOrigins.split(","));
        }
    }
}
