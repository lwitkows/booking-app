package com.lukaszwitkowski.bookingapp.booking.domain;

/**
 * Created by lwitkowski on 10.04.2018
 */
public enum BookingStatus {
    PENDING, CONFIRMED, REJECTED
}
