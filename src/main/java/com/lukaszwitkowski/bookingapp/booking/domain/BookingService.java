package com.lukaszwitkowski.bookingapp.booking.domain;

import com.lukaszwitkowski.bookingapp.booking.repository.BookingRepository;
import com.lukaszwitkowski.bookingapp.booking.repository.RoomRepository;
import com.lukaszwitkowski.bookingapp.booking.repository.RoomSearchSpecification;
import com.lukaszwitkowski.bookingapp.customer.domain.Customer;
import com.lukaszwitkowski.bookingapp.customer.repository.CustomerRepository;
import com.lukaszwitkowski.bookingapp.utils.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by lwitkowski on 10.04.2018
 */
@Service
public class BookingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingService.class);

    private final BookingRepository bookingRepository;
    private final RoomRepository roomRepository;
    private final CustomerRepository customerRepository;

    @Autowired
    public BookingService(BookingRepository bookingRepository, RoomRepository roomRepository,
                          CustomerRepository customerRepository) {
        this.bookingRepository = bookingRepository;
        this.roomRepository = roomRepository;
        this.customerRepository = customerRepository;
    }

    public Page<Room> searchRoom(RoomSearchParams searchParams, Pageable pageable) {
        if (searchParams.getDateTo().isBefore(searchParams.getDateFrom())) {
            throw new ValidationException("dateFrom must be before dateTo");
        }
        if (searchParams.getPriceFrom().isPresent() && searchParams.getPriceTo().isPresent() 
                && searchParams.getPriceFrom().get().compareTo(searchParams.getPriceTo().get()) != -1) {
            throw new ValidationException("priceFrom must be smaller than priceTo");
        }

        return roomRepository.findAll(new RoomSearchSpecification(searchParams), pageable);
    }

    @Transactional
    public Booking createBooking(Room room, Long customerId, LocalDate dateFrom, LocalDate dateTo) {
        Optional<Customer> customer = customerRepository.findById(customerId);
        if (!customer.isPresent()) {
            throw new ValidationException("Customer " + customerId + " not found");
        }
        if (dateTo.isBefore(dateFrom)) {
            throw new ValidationException("dateFrom must be before dateTo");
        }

        if (overlappingBookingExists(room, dateFrom, dateTo)) {
            throw new BookingConflictException("Room not available for this time period");
        }
        Booking booking = new Booking(customer.get(), room, dateFrom, dateTo);
        bookingRepository.save(booking);
        return booking;
    }

    @Transactional
    public void cancelBooking(Booking booking) {
        bookingRepository.delete(booking);
    }

    @Scheduled(cron = "${cron.pending.bookings.processing}")
    @Transactional
    public void processPendingBookings() {
        List<Booking> pendingBookings = bookingRepository.findFirst10ByStatus(BookingStatus.PENDING);
        if (pendingBookings.isEmpty()) return;

        LOGGER.debug("Processing pending bookings: " + pendingBookings.size());
        pendingBookings.stream().forEach(booking -> {
            if (confirmedOverlappingBookingExists(booking.getRoom(), booking.getDateFrom(), booking.getDateTo())) {
                LOGGER.debug("Rejecting booking {}", booking.getId());
                booking.reject();
            } else {
                LOGGER.debug("Confirming booking {}", booking.getId());
                booking.confirm();
            }
        });
    }

    private boolean overlappingBookingExists(Room room, LocalDate dateFrom, LocalDate dateTo) {
        long count = bookingRepository.countNotRejectedForRoomAndDateRange(room, dateFrom, dateTo);
        return count != 0l;
    }

    private boolean confirmedOverlappingBookingExists(Room room, LocalDate dateFrom, LocalDate dateTo) {
        long count = bookingRepository.countConfirmedForRoomAndDateRange(room, dateFrom, dateTo);
        return count != 0l;
    }
}