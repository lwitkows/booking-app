package com.lukaszwitkowski.bookingapp.booking.domain;

import com.lukaszwitkowski.bookingapp.customer.domain.Customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * Created by lwitkowski on 10.04.2018
 */
@Entity
@Table(name = "booking")
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    private Room room;

    @ManyToOne
    private Customer customer;

    @Column(nullable = false)
    private LocalDate dateFrom;

    @Column(nullable = false)
    private LocalDate dateTo;

    @Enumerated(EnumType.STRING)
    private BookingStatus status = BookingStatus.PENDING;

    public Booking() {
    }

    public Booking(Customer customer, Room room, LocalDate dateFrom, LocalDate dateTo) {
        this.customer = customer;
        this.room = room;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }
    public Booking(Customer customer, Room room, LocalDate dateFrom, LocalDate dateTo, BookingStatus status) {
        this(customer, room, dateFrom, dateTo);
        this.status = status;
    }
    
    public Long getId() {
        return id;
    }

    public Room getRoom() {
        return room;
    }

    public Customer getCustomer() {
        return customer;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public BookingStatus getStatus() {
        return status;
    }

    public void confirm() {
        this.status = BookingStatus.CONFIRMED;    
    }
    public void reject() {
        this.status = BookingStatus.REJECTED;
    }
}
