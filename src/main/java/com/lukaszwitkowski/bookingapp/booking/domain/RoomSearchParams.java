package com.lukaszwitkowski.bookingapp.booking.domain;

import org.springframework.lang.Nullable;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

/**
 * Created by lwitkowski on 10.04.2018
 */
public class RoomSearchParams {
    private final LocalDate dateFrom;
    private final LocalDate dateTo;
    private final Optional<BigDecimal> priceFrom;
    private final Optional<BigDecimal> priceTo;
    private final Optional<String> city;

    public static RoomSearchParams fromNullable(LocalDate dateFrom, LocalDate dateTo, 
                        @Nullable BigDecimal priceFrom, @Nullable BigDecimal priceTo, @Nullable String city) {
        return new RoomSearchParams(
                dateFrom,
                dateTo,
                Optional.ofNullable(priceFrom),
                Optional.ofNullable(priceTo),
                Optional.ofNullable(city));
    }

    public RoomSearchParams(LocalDate dateFrom, LocalDate dateTo, Optional<BigDecimal> priceFrom,
                            Optional<BigDecimal> priceTo, Optional<String> city) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.priceFrom = priceFrom;
        this.priceTo = priceTo;
        this.city = city;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public Optional<BigDecimal> getPriceFrom() {
        return priceFrom;
    }

    public Optional<BigDecimal> getPriceTo() {
        return priceTo;
    }

    public Optional<String> getCity() {
        return city;
    }
}
