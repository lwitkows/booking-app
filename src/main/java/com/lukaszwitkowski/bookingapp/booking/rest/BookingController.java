package com.lukaszwitkowski.bookingapp.booking.rest;

import com.lukaszwitkowski.bookingapp.booking.domain.Booking;
import com.lukaszwitkowski.bookingapp.booking.domain.BookingService;
import com.lukaszwitkowski.bookingapp.booking.domain.Room;
import com.lukaszwitkowski.bookingapp.booking.domain.RoomSearchParams;
import com.lukaszwitkowski.bookingapp.utils.NotFoundException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by lwitkowski on 10.04.2018
 */
@RestController
public class BookingController {

    private final BookingService bookingService;

    @Autowired
    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @RequestMapping(value = "/api/v1/room/search", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Returns pageable list of available rooms matching given parameters")
    public Page<RoomTO> searchRoom(@RequestParam("dateFrom") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateFrom,
                                   @RequestParam("dateTo") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateTo,
                                   @RequestParam(value = "priceFrom", required = false) BigDecimal priceFrom,
                                   @RequestParam(value = "priceTo", required = false) BigDecimal priceTo,
                                   @RequestParam(value = "city", required = false) String city,
                                   @PageableDefault Pageable pageable) {

        RoomSearchParams params = RoomSearchParams.fromNullable(dateFrom, dateTo, priceFrom, priceTo, city);
        return this.bookingService.searchRoom(params,pageable)
                .map(RoomTO::new);
    }


    @RequestMapping(value = "/api/v1/room/{room}/book", method = RequestMethod.POST)
    @ApiOperation("Creates new pending booking")
    @ApiResponse(code = 404, message = "Room not found")
    public BookingTO createBooking(@PathVariable("room") Room room, @RequestBody @Valid CreateBookingRequest bookRequest) {
        if (room == null) {
            throw new NotFoundException("Room not found");
        }
        return BookingTO.withCustomer(
            bookingService.createBooking(room, bookRequest.getCustomer(), bookRequest.getDateFrom(), bookRequest.getDateTo())
        );
    }


    @RequestMapping(value = "/api/v1/room/{room}/booking", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Returns all bookings for given room")
    @ApiResponse(code = 404, message = "Room not found")
    public Collection<BookingTO> getRoomBookings(@PathVariable("room") Room room) {
        if (room == null) {
            throw new NotFoundException("Room not found");
        }
        return room.getBookings().stream()
                .map(BookingTO::withCustomer)
                .collect(Collectors.toList());
    }
    
    @RequestMapping(value = "/api/v1/booking/{booking}", method = RequestMethod.DELETE, produces = "application/json")
    @ApiOperation(value = "Cancels booking")
    @ApiResponse(code = 404, message = "Booking not found")
    public void cancelBooking(@PathVariable("booking") Booking booking) {
        if (booking == null) {
            throw new NotFoundException("Booking not found");
        }
        bookingService.cancelBooking(booking);
    }
}