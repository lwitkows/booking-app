package com.lukaszwitkowski.bookingapp.booking.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.lukaszwitkowski.bookingapp.booking.domain.Booking;
import com.lukaszwitkowski.bookingapp.booking.domain.BookingStatus;
import com.lukaszwitkowski.bookingapp.customer.rest.CustomerTO;

import java.time.LocalDate;

/**
 * Created by lwitkowski on 10.04.2018
 */
public class BookingTO {
    private final Long id;
    private final LocalDate dateFrom;
    private final LocalDate dateTo;
    private final BookingStatus status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final RoomTO room;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final CustomerTO customer;
    
    public static BookingTO withCustomer(Booking booking) {
        return new BookingTO(booking, false, true);
    }
    public static BookingTO withRoom(Booking booking) {
        return new BookingTO(booking, true, false);
    }
    
    private BookingTO(Booking booking, boolean includeRoom, boolean includeCustomer) {
        this.id = booking.getId();
        this.dateFrom = booking.getDateFrom();
        this.dateTo = booking.getDateTo();
        this.status = booking.getStatus();
        if(includeRoom) {
            this.room = new RoomTO(booking.getRoom());
        } else {
            this.room = null; 
        }
        if(includeCustomer) {
            this.customer = new CustomerTO(booking.getCustomer());
        } else {
            this.customer = null; 
        }
    }

    public Long getId() {
        return id;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public CustomerTO getCustomer() {
        return customer;
    }

    public BookingStatus getStatus() {
        return status;
    }

    public RoomTO getRoom() {
        return room;
    }
}
