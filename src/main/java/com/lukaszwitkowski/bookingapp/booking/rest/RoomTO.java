package com.lukaszwitkowski.bookingapp.booking.rest;

import com.lukaszwitkowski.bookingapp.booking.domain.Room;

/**
 * Created by lwitkowski on 10.04.2018
 */
public class RoomTO {
    private final Long id;
    private final HotelTO hotel;
    private final Long price;

    public RoomTO(Room room) {
        this.id = room.getId();
        this.hotel = new HotelTO(room.getHotel());
        this.price = room.getPrice();
    }

    public Long getId() {
        return id;
    }

    public Long getPrice() {
        return price;
    }

    public HotelTO getHotel() {
        return hotel;
    }
}
