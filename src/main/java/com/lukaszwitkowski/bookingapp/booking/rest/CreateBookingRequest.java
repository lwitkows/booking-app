package com.lukaszwitkowski.bookingapp.booking.rest;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * Created by lwitkowski on 10.04.2018.
 */
public class CreateBookingRequest {

    @NotNull
    private Long customer;
    @NotNull
    private LocalDate dateFrom;
    @NotNull
    private LocalDate dateTo;

    public CreateBookingRequest() {
    }

    public Long getCustomer() {
        return customer;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }
}
