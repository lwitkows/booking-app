package com.lukaszwitkowski.bookingapp.booking.rest;

import com.lukaszwitkowski.bookingapp.booking.domain.Hotel;

/**
 * Created by lwitkowski on 10.04.2018
 */
public class HotelTO {
    private final Long id;
    private final String city;

    public HotelTO(Hotel hotel) {
        this.id = hotel.getId();
        this.city = hotel.getCity();
    }

    public Long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }
}
