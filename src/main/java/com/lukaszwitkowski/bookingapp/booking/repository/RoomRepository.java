package com.lukaszwitkowski.bookingapp.booking.repository;

import com.lukaszwitkowski.bookingapp.booking.domain.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lwitkowski on 10.04.2018.
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
    Page<Room> findAll(Specification<Room> specification, Pageable pageable);
}
