package com.lukaszwitkowski.bookingapp.booking.repository;

import com.lukaszwitkowski.bookingapp.booking.domain.Booking;
import com.lukaszwitkowski.bookingapp.booking.domain.BookingStatus;
import com.lukaszwitkowski.bookingapp.booking.domain.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by lwitkowski on 10.04.2018.
 */
@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
    @Query("SELECT count(b) FROM Booking b WHERE b.room = ?1 AND b.status != 'REJECTED' AND ((b.dateFrom BETWEEN ?2 AND ?3) OR (?2 BETWEEN b.dateFrom AND b.dateTo))")
    long countNotRejectedForRoomAndDateRange(Room room, LocalDate dateFrom, LocalDate dateTo);

    @Query("SELECT count(b) FROM Booking b WHERE b.room = ?1 AND b.status = 'CONFIRMED' AND ((b.dateFrom BETWEEN ?2 AND ?3) OR (?2 BETWEEN b.dateFrom AND b.dateTo))")
    long countConfirmedForRoomAndDateRange(Room room, LocalDate dateFrom, LocalDate dateTo);
    
    List<Booking> findFirst10ByStatus(BookingStatus status);
}
