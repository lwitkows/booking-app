package com.lukaszwitkowski.bookingapp.booking.repository;

import com.lukaszwitkowski.bookingapp.booking.domain.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lwitkowski on 10.04.2018.
 */
@Repository
public interface HotelRepository extends JpaRepository<Hotel, Long> {
}