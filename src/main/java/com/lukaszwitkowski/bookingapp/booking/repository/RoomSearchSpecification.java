package com.lukaszwitkowski.bookingapp.booking.repository;

import com.lukaszwitkowski.bookingapp.booking.domain.Booking;
import com.lukaszwitkowski.bookingapp.booking.domain.Room;
import com.lukaszwitkowski.bookingapp.booking.domain.RoomSearchParams;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lwitkowski on 10.04.2018
 */
public class RoomSearchSpecification implements Specification<Room> {
    private final RoomSearchParams searchParams;

    public RoomSearchSpecification(RoomSearchParams searchParams) {
        this.searchParams = searchParams;
    }

    @Override
    public Predicate toPredicate(Root<Room> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
        final List<Predicate> predicates = new ArrayList<>();
        
        final Subquery<Long> occupiedRooms = criteriaQuery.subquery(Long.class);
        final Join<Room, Booking> booking = occupiedRooms.from(Room.class).join("bookings");
        occupiedRooms
                .select(booking.get("room"))
                .where(
                    cb.or(
                        cb.between(booking.get("dateFrom"), searchParams.getDateFrom(), searchParams.getDateTo()),
                        cb.between(booking.get("dateTo"), searchParams.getDateFrom(), searchParams.getDateTo())
                    )
                );
        predicates.add(cb.not(cb.in(root.get("id")).value(occupiedRooms)));

        searchParams.getPriceFrom().ifPresent(priceFrom -> predicates.add(cb.greaterThanOrEqualTo(root.get("price"), priceFrom)));
        searchParams.getPriceTo().ifPresent(priceTo -> predicates.add(cb.lessThanOrEqualTo(root.get("price"), priceTo)));
        
        searchParams.getCity().ifPresent(city -> predicates.add(cb.equal(root.get("hotel").get("city"), city)));

        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
